import {LitElement,html}from 'lit-element';

class PersonaFichaListado extends LitElement{
static get properties(){
    return{
       fname: {type: String},
       yearsInCompany: {type: String},
       importe:{type:Number},
       profile:{type:String},
       //photo:{type: Object}
       photoImagenD:{type: String},
       photoAltD:{type: String}
       

    };
}
constructor(){
super ();
}
render(){
    return html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <div class="card h=100">
        <img src="${this.photoImagenD}" alt="${this.photoAltD}" height="250" width="50" class="card-img-top"/>
          <div class="card-body">
                    <h3 class="card-title">${this.fname}</h3>
                    <p class="card-text">${this.profile}</p>
                    <p class="card-text"><Strong>Nº cuenta: </Strong> ${this.yearsInCompany}</p>
                    <p class="card-text">Saldo: ${this.importe} € </p>

        </div>
        <div class="card-footer">
        
           <button @click="${this.deletePerson}" class="btn btn-danger col-5"><strong>Borrar Cliente</strong></button>
           <button @click="${this.moreInfo}" class="btn btn-info col-5 offset-1"><strong>Editar Cliente</strong></button>

        </div>
    </div>
    `;
}

moreInfo(e){
console.log("moreinfo" + this.fname);
this.dispatchEvent(
    new CustomEvent("more-info",{

    detail:{
        name: this.fname
    }

    })

);
}

deletePerson(e){

console.log("deletePerson en persona-ficha-listado");
console.log("Se va a borrar la persona de nombre " + this.fname);
this.dispatchEvent(
    new CustomEvent("delete-person",{

    detail:{
        name: this.fname
    }

    })

);
}

}
customElements.define('persona-ficha-listado', PersonaFichaListado)