import {LitElement,html,css}from 'lit-element';

class TestBootStrap extends LitElement{

    static get styles(){

        return css`
        .redbg{
              background-color: red;
        }
        .greenbg{
            background-color: green;
        }
        .bluebg{
            background-color:blue;
        }
        .greybg{
            background-color: grey;
        }
        `;
    }
render(){
    return html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <h3>Test BootStrap<h3>
    <div class="row greybg">
              <div class = "col-2 offset-1 redbg">Col 1</div>
              <div class = "col-8 offset-2 greenbg">Col 2</div>
              <div class = "col-6 offset-3 bluebg">Col 3</div>
    </div>
    `;
}
}
customElements.define('testbootstrap', TestBootStrap)