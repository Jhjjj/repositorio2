import {LitElement,html}from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';
import '../api-clientes/api-clientes.js';

class PersonaMain extends LitElement{


static get properties(){

    return{
        people:{type: Array},
        showPersonForm: {type:Boolean}
    };
}
constructor(){
super ();
this.people = [{}];
this.showPersonForm=false;
}
updated(changedProperties){

console.log("updated");
if(changedProperties.has("showPersonForm")){
   console.log("Ha cambiado el valor de la propiedad ShowPersonForm en persona-main");
   if(this.showPersonForm===true){
        this.showPersonFormData();
    }else{
    this.showPersonList();
   }
}
    if(changedProperties.has("people")){
        console.log("Ha cambiado el valor de la propiedad de people de persona-main");
        this.dispatchEvent(
            new CustomEvent("updated-people",{
                       detail: { people:this.people}})
        );
    }
}
showPersonList(){
    console.log("MostrandoListadoPersonas");
    this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
    this.shadowRoot.getElementById("personForm").classList.add("d-none");
}
showPersonFormData(){
    console.log("MostrandoListadoPersonas");
    this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    this.shadowRoot.getElementById("personForm").classList.remove("d-none");
}
    render(){
        return html`
        <api-clientes></api-clientes>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
        <div class="row" id="peopleList">
            <div class="row row-cols-1 row-cols-sm-4">
            <api-clientes @clientes-recibidos=${this.UpdateAPIclientes}></api-clientes> 
            ${this.people.map(
                person => html
                                `  
                                <persona-ficha-listado 
                                @delete-person="${this.deletePerson}"
                                @more-info="${this.infoPerson}"
                                fname="${person.name}"
                                yearsInCompany="${person.yearsInCompany}"
                                profile="${person.profile}"
                                photoImagenD="${person.photoImagen}"
                                photoAltD="${person.photoAlt}"
                                importe="${person.importe}">  
                                </persona-ficha-listado>`
            )}
            </div>
        </div>
      <div class="row">
            <persona-form 
            @store-Person="${this.personFormStore}"
            @persona-form-close="${this.personFormClose}" 
            id="personForm" class="d-none boder rounded border-primary">
            </persona-form>
      </div>          
    `;
    } 


    UpdateAPIclientes(e){

           console.log("POR FIN");
           this.people=e.detail.clientes;
           //console.log(this.people);

    }

    deletePerson(e){
        console.log("deletePerson en persona-main");
        this.people = this.people.filter(
            person => person.name!=e.detail.name
        );
    }
    personFormClose(e){
        console.log("Se ha cerrado el formulario de la persona");
        this.showPersonForm=false;
    }
    personFormStore(e){
        console.log("Guardando datos Persona en personamain");
        console.log(e.detail.person);
        if(e.detail.editingPerson === true){
                  console.log("Se va a actualizar la persona de nombre takata");

             this.people.map(
                 person => person.name === e.detail.person.name 
                    ? person=e.detail.person : person

             );
        }else{
            console.log("Se va a actualizar la persona de nombre tokoto");
            this.people = [...this.people, e.detail.person];
            console.log("Persona almacenada");

        }
        console.log("¿HAS LLEGADO AL EVENTO?"+ e.detail.person.name);
        this.showPersonForm=false;
        this.shadowRoot.querySelector("api-clientes").nuevoCliente=e.detail.person;


    }
    infoPerson(e){
      console.log("Se ha pedido info de" + e.detail.name);

      let chosenPerson =this.people.filter(
          person => person.name === e.detail.name      
      );
      // console.log(chosePerson);
      this.shadowRoot.getElementById("personForm").person = chosenPerson[0];
      this.shadowRoot.getElementById("personForm").editingPerson = true;
      this.showPersonForm=true;
    }
}
customElements.define('persona-main', PersonaMain)