import {LitElement,html}from 'lit-element';

class PersonaForm extends LitElement{


static get properties(){

    return{
        person:{type:Object},
        editingPerson:{type: Boolean}
    };
}
constructor(){
super ();
this.person={};
this.resetFormData();

}


render(){
    return html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <div>
        <form>
            <div class="form-group">
                <label>Nombre Completo</label>
                <input type="text" @input="${this.updateName}" .value="${this.person.name}" ?disabled="${this.editingPerson}" id="personforName" class="form-control" placeholder="Nombre Completo" />
            </div>
            <div class="form-group">
                <label>Perfil</label>
                <textarea @input="${this.updateProfile}" .value="${this.person.profile}" class="form-control" placeholder="Perfil" rows="5"></textarea>
            </div>
            <div class="form-group">
                <label>Nº de cuenta</label>
                <input type="text" @input="${this.updateYears}" .value="${this.person.yearsInCompany}" class="form-control" placeholder="Nº de Cuenta" />
            </div>
            <div class="form-group">
                <label>Importe</label>
                <input type="text" @input="${this.updateImporte}" .value="${this.person.importe}" class="form-control" placeholder="Importe" />
            </div>
            <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
            <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
        </form>
    </div>
    `;
}

updateImporte(e){
    console.log("Importe");
    this.person.importe=e.target.value;
    }

updateYears(e){
    console.log("Actualizando la propiedad name con el valor"+e.target.value);
    this.person.yearsInCompany=e.target.value;
    }

updateProfile(e){
console.log("Actualizando la propiedad name con el valor"+e.target.value);
this.person.profile=e.target.value;
}


updateName(e){
console.log("Actualizando la propiedad name con el valor"+e.target.value);
this.person.name=e.target.value;
}


storePerson(e){
console.log("storeperson");
e.preventDefault();

this.person.photoImagen="../src/img/Carlos.jpg";
this.person.photoAlt="Foto persona";


this.dispatchEvent(new CustomEvent("store-Person",{
              detail:{
                    person:{
                        name:this.person.name,
                        profile:this.person.profile,
                        yearsInCompany:this.person.yearsInCompany,
                        photoImagen:this.person.photoImagen,
                        photoAlt:this.person.photoAlt,
                        importe:this.person.importe
                    }
                }


            })
);
}

goBack(e){
    e.preventDefault();
    this.resetFormData();
    this.dispatchEvent(new CustomEvent("persona-form-close",{})); 
}
resetFormData(){
console.log("reset form data");
this.person={};
this.person.name="";
this.person.yearsInCompany="";
this.person.profile="";
this.person.importe="";
this.editingPerson=false;
}
}
customElements.define('persona-form', PersonaForm)