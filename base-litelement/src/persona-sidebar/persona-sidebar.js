import {LitElement,html}from 'lit-element';

class PersonaSidebar extends LitElement{


static get properties(){

    return{
        peopleStats:{type:Object}
        
    };
}
constructor(){
super ();
this.peopleStats ={};
}


render(){
    return html`
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <aside>
    <section>
    <div class="mt-5">
        <h3> <strong>Hay sólo <Strong></h3><span class="badge badge-pill badge-secondary">${this.peopleStats.numberOfPeople}</span><h3><strong> clientes</strong></h3>
            <button id="botoneliminar" @click="${this.newPerson}" class="w-50 btn bg-success">Añadir Cliente</strong></button>
        </div>

    </section>
    </aside>
    `;
}


newPerson(e){
    console.log("newPerson en persona-sidebar");
    console.log("se va a crear una nueva persona");
    this.dispatchEvent(new CustomEvent("new-person",{}));

}

}
customElements.define('persona-sidebar', PersonaSidebar)