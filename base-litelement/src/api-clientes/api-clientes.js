import {LitElement,html}from 'lit-element';
import '../persona-main/persona-main.js';

class ApiClientes extends LitElement{

static get properties(){
    return{
        clientes:{type:Array}, 
        nuevoCliente:{type:Object},
        cadenadatos:{type:String}
    };
}
constructor(){
super ();
this.clientes=[];
this.getClientes();
//this.cadenadatos="{'id':'007','name':'Manuel','profile':'Empresario','photoImagen':'src/img/Jose.jpg','photoAlt':'Jose','yearsInCompany':'ES59 0182 1234 1234 1234 1231','importe':5000000}";
//this.postClientes();
}
render(){
    return html`
    `;
}
updated (changedProperties){
    console.log("updated bueno 1");
    if(changedProperties.has("nuevoCliente")){
        console.log("update en nuevo cliente en API-CLIENTE");  
        this.postClientes(this.nuevoCliente);
    }
}

getClientes(){
      let xhr = new XMLHttpRequest();
         xhr.onload=() => {
             if(xhr.status === 200){
                 let APIResponse = JSON.parse(xhr.responseText);
                 console.log("Entra por aqui 1"+xhr.responseText);
                 this.clientes = APIResponse;
                 this.dispatchEvent(
                    new CustomEvent("clientes-recibidos",{
                               detail: { clientes:this.clientes}})
                );
            }
      }
      xhr.open("GET", "http://localhost:8081/apiHackaton/v1/personas");
      xhr.send();
}

postClientes(nuevoCliente){
    console.log("Nuevo postClientes");
     let xhr = new XMLHttpRequest();
     xhr.open("POST", "http://localhost:8081/apiHackaton/v1/personas");
     xhr.setRequestHeader("Content-type","application/json");
     xhr.
     setRequestHeader("body",JSON.stringify(this.nuevoCliente));
     xhr.send();
}

}
customElements.define('api-clientes', ApiClientes)